# TSI-Météo /Frontend
Cette application web représente la partie frontend du projet TSI-Météo. Il s'agit d'un dashboard de visualisation des données des 3 sondes réalisées pour le projet JavaScript avancé (TSI 2018/2019, Ecole Nationale des Sciences Géographiques).
(backend: YSHMeteoApp https://gitlab.com/Yassmine.Boudili/yshmeteoapp)

### Installation
```
npm install
npm install -g serve
```
### Compilation pour la production
```
npm run build
```

### Déploiement
```
serve -s dist
```
Finalement vous pouvez accéder à l'application depuis l'adresse résultante du déploiement.

### Auteurs
Yassmine BOUDILI - Hiba GHADHAB - Sinda THAALBI.  
<br/>
Janvier 2019.