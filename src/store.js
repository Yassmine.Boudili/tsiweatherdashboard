import Vue from 'vue'
import Vuex from 'vuex'
import Moment from 'moment'

Vue.use(Vuex)

export default new Vuex.Store({

  state: {

    allSondes: [
      { text: 'Sonde YSH', value: 'piensg010:3001', isSelected: true },
      { text: 'Sonde MCM', value: 'piensg009:3001', isSelected: false },
      { text: 'Sonde BMW', value: 'piensg011:3001', isSelected: false }
    ],
    allCapteurs: [
      { text: 'Temperature', value: 'temp', unit: '°C' },
      { text: 'Pressure', value: 'press', unit: 'hP' },
      { text: 'Humidity', value: 'hygro', unit: '%' },
      { text: 'Illuminance', value: 'lum', unit: 'Lux' },
      { text: 'Precipitations', value: 'rain', unit: 'mm' },
      { text: 'Wind', value: 'wind', unit: 'Kts' }
    ],

    windRose: ['NORTH', 'NNE', 'NE', 'ENE', 'EAST', 'ESE', 'SE', 'SSE', 'SOUTH', 'SSW', 'SW', 'WSW', 'WEST', 'WNW', 'NW', 'NNW'],

    colorPalette: ['#fc4a1a', '#4abdac', '#f7b733', '#dfdce3', '#495057', '#262228'],

    liveAllMeasures: {},
    liveComparativeMeasures: [], // for charts
    historicMeasures: [], // for charts
    historicWindMeasures: [], // for wind rose
    markers: []

  },

  mutations: {

    setLiveAllMeasures (state, resultats) {
      // set isSelected attribute to match the selected sonde
      state.allSondes.forEach(e => {
        if (e.value !== resultats[0]) e.isSelected = false
        else e.isSelected = true
      })
      state.liveAllMeasures = resultats[1]
    },

    setLocations (state, resultats) {
      state.markers = resultats
    },

    setLiveComparativeMeasures (state, resultats) {
      state.liveComparativeMeasures = resultats
    },

    clearLiveComparativeMeasures (state) {
      state.liveComparativeMeasures = []
    },

    setHistoricMeasures (state, resultats) {
      state.historicMeasures = resultats
    },

    clearHistoricMeasures (state) {
      state.historicMeasures = []
    },

    setHistoricWindMeasures (state, resultats) {
      state.historicWindMeasures = resultats
    },

    clearHistoricWindMeasures (state) {
      state.historicWindMeasures = []
    }

  },

  actions: {
    /**
     * Fetch and commit the last values of all sensors for the selected sonde
     * @param {string} sonde selected sonde
     */
    getLiveAllMeasures ({ commit, state }, sonde) {
      fetch('http://' + sonde + '/last?capteur_type=all')
        .then((result) => { return result.json() })
        .then((result) => {
          if (result.measurements !== undefined) { this.commit('setLiveAllMeasures', [sonde, result.measurements]) } else { this.commit('setLiveAllMeasures', [sonde, {}]) }
        })
        .catch((e) => {
          this.commit('setLiveAllMeasures', [sonde, {}])
        })
    },

    /**
     * Fetch and commit the last values of the selected sensor for all sondes
     * @param {string} capteurType selected sensor tab
     */
    getLiveComparativeMeasures ({ commit, state }, capteurType) {
      this.commit('clearLiveComparativeMeasures') // clear previous measures to clear the chart
      var capteurTypes = []; var requestParams = ''
      // if selected tab is 'wind':
      //    fetch all last measures and select the three value [mean,min,max] (for stacked bar chart)
      if (capteurType === 'wind') {
        capteurTypes = ['wind_min', 'wind_mean', 'wind_max'];
        requestParams = 'all';
      }
      // else: fetch only the value of the corresponding sensor (for simple bar chart)
      else {
        capteurTypes = [capteurType];
        requestParams = capteurType;
      }
      var promises = []
      // for each sonde fetch the last measurements
      for (let i = 0; i < this.state.allSondes.length; i++) {
        promises.push(
          fetch('http://' + this.state.allSondes[i].value + '/last?capteur_type=' + requestParams)
            .then(result => result.json())
            .catch(() => null)
        )
      }
      // requests are completed for the three sondes
      Promise.all(promises)
        .then(promises => {
          var measures = []
          // for each sensor ("min max and mean in case of wind" OR "the one selected sensor otherwise")
          capteurTypes.forEach((capt, j) => {
            measures.push({ type: capt, data: [] })
            for (let i = 0; i < promises.length; i++) {
              if (promises[i] !== null) {
                if (promises[i].measurements[capt] !== undefined) { measures[j].data.push(promises[i].measurements[capt]) } // push the value of the current sonde
              } else measures[j].data.push(null)
            }
          })
          this.commit('setLiveComparativeMeasures', measures)
        })
    },

    /**
     * Fetch and commit the archived measures of the selected sensor for all sondes in a given period
     * @param {Array} params [ selected sensor, start timestamp(s), end timestamp(s)]
     */
    getHistoricMeasures ({ commit, state }, params) {
      this.commit('clearHistoricMeasures') // clear previous measures to clear the chart
      var [capteurType, startDate, endDate] = [ params[0], params[1], params[2] ]
      var promises = []
      for (let i = 0; i < this.state.allSondes.length; i++) {
        promises.push(
          fetch('http://' + this.state.allSondes[i].value + '/period?capteur_type=' + capteurType + '&dateStart=' + startDate + '&dateEnd=' + endDate)
            .then(result => result.json())
            .catch(() => null)
        )
      }
      // requests are completed for the three sondes
      Promise.all(promises)
        .then(promises => {
          var historicMeasures = []
          for (let i = 0; i < promises.length; i++) {
            if (promises[i] !== null) {
              if (capteurType !== 'rain') {
                // extract X Y data from each measurement object
                var data = promises[i].data.map(e => {
                  return { x: e.measurements.date, y: e.measurements[capteurType] }
                })
                // push the measurements of each sonde as a reduced X Y dataset
                historicMeasures.push({
                  sonde_name: promises[i].name,
                  data: reduce(data, 250), // reduce data to array of 250 max for chart rendering
                  type: 'line' // type of the chart
                })
              } else {
                // push the calculated precipitations for each sonde
                historicMeasures.push({
                  sonde_name: promises[i].name,
                  // cummulate precipitations on 24 intervals and return an X Y dataset
                  data: calculatePrecipitations(promises[i].rain, startDate, endDate, 24),
                  type: 'bar' // type of the chart
                })
              }
            }
          }
          this.commit('setHistoricMeasures', historicMeasures)
        })
    },

    /**
     * Fetch and commit the aggregated measures of wind speed and direction for all sondes in a given period
     * @param {Array} params [start timestamp, end timestamp]
     */
    getHistoricWindMeasures ({ commit, state }, params) {
      this.commit('clearHistoricWindMeasures') // clear previous measures to clear the chart
      var [startDate, endDate] = [ params[0], params[1] ]
      var promises = []
      for (let i = 0; i < this.state.allSondes.length; i++) {
        promises.push(
          fetch('http://' + this.state.allSondes[i].value + '/period?capteur_type=all&dateStart=' + startDate + '&dateEnd=' + endDate)
            .then(result => result.json())
            .catch(() => null)
        )
      }
      // requests are completed for the three sondes
      Promise.all(promises)
        .then(promises => {
          var historicWindMeasures = []
          for (let i = 0; i < promises.length; i++) {
            if (promises[i] !== null) {
              // extract wind speed and direction from each measurement object
              var data = promises[i].data.map(e => {
                return { dir: e.measurements.wind_dir, speed: e.measurements.wind_mean }
              })
              // push the aggregated wind speeds for each sonde
              historicWindMeasures.push({
                sonde_name: promises[i].name,
                data: aggregateWind(data)
              })
            }
          }
          this.commit('setHistoricWindMeasures', historicWindMeasures)
        })
    },

    /**
     * Fetch and commit the last lat lng positions of all sondes
     */
    getLocations ({ commit, state }) {
      var promises = []
      for (let i = 0; i < this.state.allSondes.length; i++) {
        promises.push(
          fetch('http://' + this.state.allSondes[i].value + '/last?capteur_type=location')
            .then(result => result.json())
            .catch(() => null)
        )
      }
      // requests are completed for the three sondes
      Promise.all(promises)
        .then(promises => {
          var locations = []
          for (let i = 0; i < promises.length; i++) {
            if (promises[i] !== null) {
              if (promises[i].location !== undefined) {
              // push the lat lng positions for each sonde
                locations.push({
                  id: promises[i].is,
                  position: { lng: Number(promises[i].location.lng), lat: Number(promises[i].location.lat) },
                  tooltip: promises[i].name // name of the sonde as a tooltip for the marker
                })
              }
            }
          }
          this.commit('setLocations', locations)
        })
    }

  }
})

/**
 * Reduce the length of a X Y dataset
 * @param {Array} data Array of {x y} data
 * @param {Number} maxCount Maximum length of reduced result
 */
function reduce (data, maxCount) {
  if (data.length <= maxCount) { return data }
  var blockSize = data.length / maxCount // size of the divided chunks
  var reduced = []
  for (var i = 0; i < data.length;) {
    var chunk = data.slice(i, (i += blockSize) + 1) // divide input data
    reduced.push(average(chunk)) // push the average X Y values of the divided chunk
  }
  return reduced
}

/**
 * Return the average X and Y values of an array
 * @param {Array} chunk Array of {x y} data, x being a date/time
 */
function average (chunk) {
  var x = 0
  var y = 0
  for (var i = 0; i < chunk.length; i++) {
    x += Moment(chunk[i].x)
    y += chunk[i].y
  }
  return {
    x: Moment(x / chunk.length).format('YYYY-MM-DD HH:mm:ss'),
    y: (y / chunk.length).toFixed(2)
  }
}

/**
 * Calculate the average wind speed by wind direction
 * @param {Array} data array of {dir, speed} (direction and speed of wind)
 * @returns {Array} array of wind mean speed values for 16 direction of the wind (from 0° to 360° , with 22.5° step)
 */
function aggregateWind (data) {
  var results = new Array(16) // 22.5° step wind rose (360°/16)

  for (let i = 0; i < data.length; i++) {
    const index = Math.round(data[i].dir / 22.5) // rounding the wind direction angle to match the 22.5° step scale
    const indexDir = index >= 16 ? 0 : index // index of the current direction on the wind rose

    if (results[indexDir] === undefined) { results[indexDir] = { sum: data[i].speed, count: 1 } } // initializing the speed of the corresponding direction

    results[indexDir].sum += data[i].speed // summing the speeds
    results[indexDir].count += 1 // incrementing the count of the summed speeds
  }

  return results.map((e) => { return (e.sum / e.count).toFixed(3) }) // return the average speed for each direction
}

/**
 * Claculate the precipitations during a given period cummulated on a number of intervals
 * @param {Array} data array of dates corresponding each to 2ml of rain
 * @param {Number} startDate begining of the period (timestamp in seconds)
 * @param {Number} endDate end of the period (timestamp in seconds)
 * @param {Number} nbrIntervals number of intervals that divide the period
 * @returns {Array} array of {x y}, data x being the date/time and y the cummulated rain in mm
 */
function calculatePrecipitations (data, startDate, endDate, nbrIntervals) {
  startDate = Moment(startDate * 1000)
  endDate = Moment(endDate * 1000)
  var step = (endDate - startDate) / nbrIntervals
  var result = []
  for (let i = 0; i < nbrIntervals; i++) {
    // setting the interval
    var interval = [ startDate + (step * i), startDate + (step * (i + 1)) ]
    // counting the number of 2ml recorded in the current interval
    var count = data.filter((e) => { return (interval[0] <= Moment(e) && Moment(e) <= interval[1]) }).length

    result.push({
      x: Moment((interval[0] + interval[1]) / 2).format('YYYY-MM-DD HH:mm:ss'), // positioning the bar on the middle of the interval
      y: (count / 2000) // converting the cummulated 2ml to mm
    })
  }
  return result
}
